<?php

namespace Drupal\bifm_cpd\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Utility\Xss;

/**
 * Edit employment record.
 */
class EmploymentEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cpd_employee_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Query database table to get this employment data.
    $result = db_select('bifm_employment_records', 'er')
      ->fields('er')
      ->condition('er.id', $edit_id[4], '=')
      ->execute()
      ->fetchObject();

    // Define form fields.
    $form['form_heading_1'] = [
      '#markup' => $this->t('<h2>Employment Record (Edit)</h2>'),
    ];

    $form['form_heading_2'] = [
      '#markup' => $this->t('<b>Form Details (* = mandatory fields):</b>'),
    ];

    $form['job_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Job Title'),
      '#default_value' => $result->job_title,
      '#required' => TRUE,
    ];

    $level_of_seniority_options = [
      'Manager' => 'Manager',
      'Senior' => 'Senior',
      'Strategic' => 'Strategic',
      'Supervisor' => 'Supervisor',
      'Support' => 'Support',
    ];

    $form['level_of_seniority'] = [
      '#type' => 'select',
      '#title' => $this->t('Level of Seniority'),
      '#default_value' => $result->level_of_seniority,
      '#options' => $level_of_seniority_options,
      '#required' => TRUE,
    ];

    $form['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#default_value' => $result->company,
      '#required' => TRUE,
    ];

    $form['start_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start Date'),
      '#default_value' => DrupalDateTime::createFromTimestamp($result->start_date),
      '#required' => TRUE,
    ];

    $form['end_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End Date'),
      '#default_value' => DrupalDateTime::createFromTimestamp($result->end_date),
      '#required' => TRUE,
    ];

    $form['summary_of_responsibilities'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Summary of Responsibilities'),
      '#default_value' => $result->summary_of_responsibilities,
      '#required' => TRUE,
    ];

    $form['achievements'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Achievements'),
      '#default_value' => $result->achievements,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {
  if ($form_state->getValue('job_title') == NULL) {
  $form_state->setErrorByName('job_title', $this->t('Job Title.'));
  }
  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Save posted data in the database table.
    db_merge('bifm_employment_records')
      ->key(['id' => $edit_id[4]])
      ->fields([
        'member_id' => $this->currentUser()->id(),
        'job_title' => Xss::filter($form_state->getValue('job_title')),
        'level_of_seniority' => Xss::filter($form_state->getValue(
        'level_of_seniority')),
        'company' => Xss::filter($form_state->getValue('company')),
        'start_date' => Xss::filter(strtotime($form_state->getValue('start_date'))),
        'end_date' => Xss::filter(strtotime($form_state->getValue('end_date'))),
        'summary_of_responsibilities' => Xss::filter($form_state->getValue(
        'summary_of_responsibilities')),
        'achievements' => Xss::filter($form_state->getValue('achievements')),
      ])
      ->execute();

    // Display success message.
    drupal_set_message($this->t('Employment record successfully edited.'));

    // Redirect.
    // $form_state->setRedirect('bifm_cpd.employee_add');.
  }

}
