<?php

namespace Drupal\bifm_cpd\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Component\Utility\Xss;

/**
 * Add employment record.
 */
class EmploymentAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cpd_employee_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Define form fields.
    $form['form_heading_1'] = [
      '#markup' => $this->t('<h2>Employment Record (Add)</h2>'),
    ];

    $form['form_heading_2'] = [
      '#markup' => $this->t('<b>Form Details (* = mandatory fields):</b>'),
    ];

    $form['job_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Job Title'),
      '#required' => TRUE,
    ];

    $level_of_seniority_options = [
      '' => 'Select Career Level',
      'Manager' => 'Manager',
      'Senior' => 'Senior',
      'Strategic' => 'Strategic',
      'Supervisor' => 'Supervisor',
      'Support' => 'Support',
    ];

    $form['level_of_seniority'] = [
      '#type' => 'select',
      '#title' => $this->t('Level of Seniority'),
      '#options' => $level_of_seniority_options,
      '#required' => TRUE,
    ];

    $form['company'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Company'),
      '#required' => TRUE,
    ];

    $form['start_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start Date'),
      '#default_value' => DrupalDateTime::createFromTimestamp(time()),
      '#required' => TRUE,
    ];

    $form['end_date'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End Date'),
      '#default_value' => DrupalDateTime::createFromTimestamp(time()),
      '#required' => TRUE,
    ];

    $form['summary_of_responsibilities'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Summary of Responsibilities'),
      '#required' => TRUE,
    ];

    $form['achievements'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Achievements'),
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {
  if ($form_state->getValue('job_title') == NULL) {
  $form_state->setErrorByName('job_title', $this->t('Job Title.'));
  }
  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Current date time.
    $created = date('Y-m-d H:i:s');

    // Save posted data in the database table.
    db_insert('bifm_employment_records')
      ->fields([
        'member_id' => $this->currentUser()->id(),
        'job_title' => Xss::filter($form_state->getValue('job_title')),
        'level_of_seniority' => Xss::filter($form_state->getValue(
        'level_of_seniority')),
        'company' => Xss::filter($form_state->getValue('company')),
        'start_date' => Xss::filter(strtotime($form_state->getValue('start_date'))),
        'end_date' => Xss::filter(strtotime($form_state->getValue('end_date'))),
        'summary_of_responsibilities' => Xss::filter($form_state->getValue(
        'summary_of_responsibilities')),
        'achievements' => Xss::filter($form_state->getValue('achievements')),
        'created' => $created,
      ])
      ->execute();

    // Display success message.
    drupal_set_message($this->t('Employment record successfully added.'));

    // Redirect.
    // $form_state->setRedirect('bifm_cpd.employee_add');.
  }

}
